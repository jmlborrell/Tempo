//
//  ViewController.swift
//  Tempo
//
//  Created by Jose Borrell on 7/29/18.
//  Copyright © 2018 Jose Borrell. All rights reserved.
//

import UIKit

class NPController: UIViewController {
    
    var npView : NPView!
    var animator : UIViewPropertyAnimator!
    var animator2 : UIViewPropertyAnimator!
    var runningAnimators = [UIViewPropertyAnimator]()
    var currentProgress : [CGFloat] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
 
        setUpView()
        
        self.npView.albumArt.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleNextPan(recognizer:))))
    }

    
    @objc func handleNextPan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            animateTransitionIfNeeded()
            currentProgress = runningAnimators.map { $0.fractionComplete }
        case .changed:
            let translation = recognizer.translation(in: self.npView.albumArt)
            for (index,animator) in runningAnimators.enumerated() {
                animator.fractionComplete = (translation.x / 2000) * -1 + currentProgress[index]
            }
        case .ended:
            let direction = recognizer.velocity(in: self.npView.albumArt)
            if (direction.x > 0) {
                runningAnimators.forEach { $0.isReversed = true }
            }
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            runningAnimators = []
        default:
            return
        }
    }
    
    fileprivate func animateTransitionIfNeeded() {
        
        guard runningAnimators.isEmpty else {return}
        animator = UIViewPropertyAnimator(duration: 2, curve: .easeIn, animations: {
            let shrink : CGAffineTransform = CGAffineTransform(scaleX: 0.06, y: 0.06)
            let move : CGAffineTransform = CGAffineTransform(translationX: -159, y: 0)
            self.npView.albumArt.transform = shrink.concatenating(move)
            self.npView.albumArt.alpha = 0
            self.npView.previousIcon.alpha = 1
        })
        animator2 = UIViewPropertyAnimator(duration: 2, curve: .easeIn, animations: {
            let enlarge : CGAffineTransform = CGAffineTransform(scaleX: 15.97, y: 15.97)
            let move : CGAffineTransform = CGAffineTransform(translationX: -159, y: 0)
            self.npView.nextAlbumArt.transform = enlarge.concatenating(move)
            self.npView.nextAlbumArt.layer.cornerRadius = 2
            self.npView.nextAlbumArt.alpha = 1
            self.npView.nextIcon.alpha = 0
        })
        
        animator.pauseAnimation()
        animator2.pauseAnimation()
        runningAnimators.append(animator)
        runningAnimators.append(animator2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    func setUpView() {
        self.npView = NPView(frame: self.view.frame)
        self.view.addSubview(npView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

