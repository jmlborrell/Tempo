//
//  ContentSlide.swift
//  Tempo
//
//  Created by Jose Borrell on 8/1/18.
//  Copyright © 2018 Jose Borrell. All rights reserved.
//

import Foundation

import UIKit

class ContentSlide : UIView {
    
    var content : [UIView]

    override init(frame: CGRect) {
        content = []
        super.init(frame: frame)
        
        backgroundColor = .white
        
        layoutIfNeeded()
        layer.cornerRadius = frame.height / 10.0
        layer.masksToBounds = true
        
        layer.shadowOffset = CGSize(width: 0, height: 13.33)
        layer.shadowRadius = CGFloat(integerLiteral: 33)
        layer.shadowOpacity = 20
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addContent(content : [UIView]) {
        for item in content {
            addSubview(item)
        }
    }
    
    
}
