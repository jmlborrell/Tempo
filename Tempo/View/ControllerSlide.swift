//
//  ControllerSlide.swift
//  Tempo
//
//  Created by Jose Borrell on 8/3/18.
//  Copyright © 2018 Jose Borrell. All rights reserved.
//

import Foundation

import UIKit

class ControllerSlide: ContentSlide {
    
    let stateButton : UIButton! = {
        let button = UIButton(frame: CGRect(x: 145.9, y: 105.5, width: 43, height: 46.7))
        if let pause = UIImage(named: "pausebutton") {
            button.setImage(pause, for: .normal)
        }
        button.addTarget(self, action: #selector(changeState), for: .touchUpInside)
        return button
    }()
    
    let timeScrub : UISlider = {
        let slider = UISlider(frame: CGRect(x: 30.6, y: 61.2, width: 270.3, height: 16.7))
        slider.minimumTrackTintColor = .purple
        if let thumb = UIImage(named: "thumbimage") {
            slider.setThumbImage(thumb, for: .normal)
        }
        return slider
    }()
    
    let volume : UISlider = {
        let slider = UISlider(frame: CGRect(x: 48.4, y: 180, width: 234.7, height: 16.7))
        slider.minimumTrackTintColor = .purple
        if let thumb = UIImage(named: "thumbimage") {
            slider.setThumbImage(thumb, for: .normal)
        }
        return slider
    }()
    
    let songName : UILabel = {
        let label = UILabel(frame: CGRect(x: 30.6, y: 5, width: 270.3, height: 34.7))
        label.text = "Taste The Feeling"
        if let font = UIFont(name: "DIN Alternate", size: CGFloat(integerLiteral: 18)) {
            label.font = font
        }
        label.textAlignment = NSTextAlignment.center
        
        return label
    }()
    
    let artistName : UILabel = {
        let label = UILabel(frame: CGRect(x: 30.6, y: 38, width: 270.3, height: 11.1))
        label.text = "Avicii Vs Conrad Sewell"
        if let font = UIFont(name: "DIN Alternate", size: CGFloat(integerLiteral: 12)) {
            label.font = font
        }
        label.textColor = .darkGray
        label.textAlignment = NSTextAlignment.center
        
        return label
    }()
    
    let loveButton : UIButton! = {
        let button = UIButton(frame: CGRect(x: 75.7, y: 118.2, width: 24, height: 21.4))
        if let pause = UIImage(named: "love") {
            button.setImage(pause, for: .normal)
        }
        return button
    }()
    
    let menuButton : UIButton! = {
        let button = UIButton(frame: CGRect(x: 224.6, y: 123.3, width: 23.4, height: 11.1))
        if let pause = UIImage(named: "menubar") {
            button.setImage(pause, for: .normal)
        }
        return button
    }()
    
    let minVolIcon : UIImageView! = {
        let view = UIImageView(frame: CGRect(x: 30.6, y: 182.3, width: 7.4, height: 12))
        if let icon = UIImage(named: "minvol") {
            view.image = icon
        }
        return view
    }()
    
    let maxVolIcon : UIImageView! = {
        let view = UIImageView(frame: CGRect(x: 293.4, y: 182.3, width: 15.8, height: 12))
        if let icon = UIImage(named: "maxvol") {
            view.image = icon
        }
        return view
    }()

    
    
    @objc func changeState() {
        if (NPModel.state.currentlyPlaying) {
            if let image = UIImage(named: "playbutton") {
                stateButton.setImage(image, for: .normal)
            }
        } else {
            if let image = UIImage(named: "pausebutton") {
                stateButton.setImage(image, for: .normal)
            }
        }
        
        NPModel.state.changeState()
    }
    
    var animator : UIViewPropertyAnimator!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        content = [stateButton,timeScrub,volume,songName,artistName,loveButton,menuButton,minVolIcon,maxVolIcon]
        addContent(content: content)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
