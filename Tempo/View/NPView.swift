//
//  NPView.swift
//  Tempo
//
//  Created by Jose Borrell on 7/29/18.
//  Copyright © 2018 Jose Borrell. All rights reserved.
//

import Foundation

import UIKit

class NPView: UIView {
    
    var currentlyPlaying : Bool
    var slides : [UIView]
    
    let profileButton : UIButton! = {
        let button = UIButton(frame: CGRect(x: 329.5, y: 47.67, width: 25.27, height: 25))
        if let image = UIImage(named: "profilelogo") {
            button.setImage(image, for: .normal)
        }
        return button
    }()
    
    let searchButton : UIButton! = {
        let button = UIButton(frame: CGRect(x: 19.97, y: 47.67, width: 25.4, height: 25))
        if let image = UIImage(named: "searchlogo") {
            button.setImage(image, for: .normal)
        }
        return button
    }()
    
    let slideButton : UIButton! = {
        let button = UIButton(frame: CGRect(x: 165.4, y: 102.87, width: 48.2, height: 7.33))
        if let image = UIImage(named: "slidedown") {
            button.setImage(image, for: .normal)
        }
        return button
    }()
    
    let albumArt : UIImageView! = {
        let iv = UIImageView(frame: CGRect(x: 54.17, y: 133.97, width: 266.67, height: 266.67))
        if let image = UIImage(named: "albumart") {
            iv.image = image
        }
        iv.layoutIfNeeded()
        iv.layer.cornerRadius = iv.frame.height / 10.0
        iv.layer.masksToBounds = true
        iv.isUserInteractionEnabled = true
        
        return iv
    }()
    
    let nextAlbumArt : UIImageView! = {
        let iv = UIImageView(frame: CGRect(x: 338.1, y: 259, width: 16.7, height: 16.7))
        if let image = UIImage(named: "albumart") {
            iv.image = image
        }
        iv.layoutIfNeeded()
        iv.layer.cornerRadius = iv.frame.height / 2.0
        
        iv.alpha = 0
        iv.layer.masksToBounds = true
        iv.isUserInteractionEnabled = true
        
        return iv
    }()
    
    let previousIcon : UIImageView! = {
        let iv = UIImageView(frame: CGRect(x: 20, y: 259, width: 16.7, height: 16.7))
        if let image = UIImage(named: "previous") {
            iv.image = image
        }
        
        iv.alpha = 0
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    let nextIcon : UIImageView! = {
        let iv = UIImageView(frame: CGRect(x: 338.1, y: 259, width: 16.7, height: 16.7))
        if let image = UIImage(named: "next") {
            iv.image = image
        }
        iv.isUserInteractionEnabled = true
        return iv
        
    }()
    
    
    let background : UIImageView! = {
        let iv = UIImageView(frame: CGRect(x: -242, y: -25.53, width: 863.07, height: 863.07))
        if let image = UIImage(named: "albumart") {
            iv.image = image
        }
        
        let blur = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blur)
        blurView.frame = iv.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        iv.addSubview(blurView)
        
        return iv
    }()
    
    override init(frame: CGRect) {
        currentlyPlaying = true
        self.slides = []
        super.init(frame: frame)
        let controllerSlide = ControllerSlide(frame: CGRect(x: 19.87, y: 418.47, width: 334.8, height: 225.4))
        slides.append(controllerSlide)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setUpViews() {
        backgroundColor = .white
        addSubview(background)
        addSubview(albumArt)
        addSubview(nextAlbumArt)
        addSubview(profileButton)
        addSubview(slideButton)
        addSubview(searchButton)
        addSubview(previousIcon)
        addSubview(nextIcon)
        addSlides()
    }
    
    fileprivate func addSlides() {
        for slide in slides {
            self.addSubview(slide)
        }
    }
}
