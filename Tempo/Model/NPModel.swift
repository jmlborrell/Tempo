//
//  NPModel.swift
//  Tempo
//
//  Created by Jose Borrell on 7/29/18.
//  Copyright © 2018 Jose Borrell. All rights reserved.
//

import Foundation

import UIKit

class NPModel {
    
    static let state = NPModel()
    var currentlyPlaying : Bool
    
    fileprivate init() {
        currentlyPlaying = false
    }
    
    func changeState() {
        self.currentlyPlaying = !currentlyPlaying
    }

}
